package ru.itmo.game;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

// Please note that on macOS your application needs to be started with the -XstartOnFirstThread JVM argument
public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("java.awt.headless", Boolean.TRUE.toString());
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setForegroundFPS(60);
		config.setTitle("Game");
		config.setWindowedMode(1280, 720);
		config.setResizable(false);
		new Lwjgl3Application(new CardGame(), config);
	}
}
