package ru.itmo.game.processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import java.util.List;
import ru.itmo.game.client.Client;
import ru.itmo.game.client.model.GameStatus;
import ru.itmo.game.client.model.MakeMoveRequest;
import ru.itmo.game.client.model.MoveType;
import ru.itmo.game.model.Card;
import ru.itmo.game.model.GameField;

public class GameProcessor {
    private static void processInput(GameField gameField) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)
            && gameField.getCurrentStatus() == GameStatus.INITIAL) {
            if (Client.startGame(gameField.getGame().getGameId())) {
                gameField.setLog("Game has started");
            }
        }
    }

    private static void updateState(GameField gameField) {
        if (System.currentTimeMillis() - gameField.getTimer() > GameField.TIME_UPDATE) {
            var state = Client.getState(gameField.getGame().getGameId());
            if (state != null) {
                var userState = Client.getStateForUser(state.getId(), gameField.getUser().getId());
                if (userState != null) {
                    if (gameField.isCurrentTurn() && !gameField.wasUpdated() || !gameField.isCurrentTurn()) {
                        gameField.setCurrentState(state);
                        gameField.setStateForUser(userState);
                        gameField.currentPlayer().cards().clear();
                        gameField.currentPlayer().cards().addAll(gameField.getCardsForPlayer());
                        gameField.getInAction().clear();
                        gameField.getInAction().addAll(gameField.getCurrentCards());
                        var chosenFace = gameField.getFaceCard();
                        if (chosenFace != null) {
                            gameField.setChosenFace(gameField.mapCard(chosenFace));
                        } else {
                            gameField.setChosenFace(null);
                        }
                        gameField.setUpdated(gameField.isCurrentTurn());
                    }
                }
            }
            gameField.setTimer(System.currentTimeMillis());
        }
    }

    private static void updateText(GameField gameField) {
        var users = gameField.getCurrentState().getPlayers();
        StringBuilder usersLog = new StringBuilder();
        for (var user : users) {
            usersLog.append(user)
                .append("\n");
        }
        gameField.setLog(String.format("""
            Users:
            %s
            """, usersLog));
    }

    public static void process(GameField gameField) {
        processInput(gameField);
        updateState(gameField);
        if (gameField.getCurrentStatus() != GameStatus.IN_PROGRESS) {
            return;
        }
        updateText(gameField);
        if (!gameField.isCurrentTurn()) {
            return;
        }
        var currentPlayer = gameField.currentPlayer();
        if (PlayerProcessor.processPlayer(gameField, currentPlayer)) {
            var chosenFace = gameField.getFaceCard();
            if (gameField.isPlayerChecking()) {
                int pickedId = 0;
                for (int i = 0; i < gameField.getInAction().size(); i++) {
                    if (!gameField.getInAction().get(i).isFlipped()) {
                        pickedId = i;
                    }
                }
                Client.makeMove(gameField.getGame().getGameId(), new MakeMoveRequest(
                    MoveType.CHECK,
                    pickedId,
                    List.of(),
                    chosenFace
                ));
                gameField.setUpdated(false);
                System.out.println(
                    gameField.getGame().getGameId() + "\n" +
                        gameField.getStateForUser().getUserTurn() + "\n" +
                    new MakeMoveRequest(
                        MoveType.CHECK,
                        pickedId,
                        List.of(),
                        chosenFace
                    )
                );
            } else {
                var chosenCards = currentPlayer.cards().stream().filter(Card::isChosen).toList();
                if (!gameField.getStateForUser().getAvailableMoves().contains(MoveType.CHECK)) {
                    chosenFace = gameField.getChosenFace().getId();
                }
                Client.makeMove(gameField.getGame().getGameId(), new MakeMoveRequest(
                    MoveType.TRANSFER,
                    null,
                    chosenCards.stream().map(Card::getId).toList(),
                    chosenFace
                ));
                System.out.println(
                    gameField.getGame().getGameId() + "\n" +
                        gameField.getStateForUser().getUserTurn() + "\n" +
                    new MakeMoveRequest(
                        MoveType.TRANSFER,
                        null,
                        chosenCards.stream().map(Card::getId).toList(),
                        chosenFace
                    )
                );
                System.out.println("Chosen: " + chosenFace);
                gameField.setUpdated(false);
            }
            gameField.setPlayerChecking(false);
        }
    }
}
