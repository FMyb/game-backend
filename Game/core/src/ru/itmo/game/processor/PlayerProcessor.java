package ru.itmo.game.processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import ru.itmo.game.client.model.MoveType;
import ru.itmo.game.model.Card;
import ru.itmo.game.model.GameField;
import ru.itmo.game.model.Player;
import ru.itmo.game.utils.GdxUtils;

public class PlayerProcessor {
    public static boolean processPlayer(GameField gameField, Player player) {
        if (gameField.isPlayerChecking()) {
            for (var card : gameField.getInAction()) {
                if (checkCollision(card)) {
                    card.flip();
                    return true;
                }
            }
        } else {
            for (var card : player.cards()) {
                if (checkCollision(card)) {
                    card.choose();
                }
            }
            for (var card : gameField.getChoosingCards()) {
                if (checkCollision(card) && gameField.isChoosingFace()) {
                    gameField.setChoosingFace(false);
                    gameField.setChosenFace(new Card(card.getRank(), card.getSuit(), card.getId()));
                    return true;
                }
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
                if (!gameField.getStateForUser().getAvailableMoves().contains(MoveType.CHECK)) {
                    gameField.setChoosingFace(true);
                } else {
                    return true;
                }
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && !gameField.getCurrentCards().isEmpty()) {
                gameField.setPlayerChecking(true);
            }
        }
        return false;
    }

    private static boolean checkCollision(Card card) {
        return Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)
            && card.checkCollision(GdxUtils.getX(), GdxUtils.getY());
    }
}
