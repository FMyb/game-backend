package ru.itmo.game.processor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import ru.itmo.game.CardGame;
import ru.itmo.game.client.Client;
import ru.itmo.game.client.model.Game;
import ru.itmo.game.screen.GamePlay;
import ru.itmo.game.screen.Menu;

public class MenuProcessor {
    private final StringBuilder textField = new StringBuilder();
    private final Menu menu;
    private int shift = 68;

    public MenuProcessor(Menu menu) {
        this.menu = menu;
        var inputAdapter = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode >= Input.Keys.A && keycode <= Input.Keys.Z) {
                    textField.append((char) (keycode + shift));
                }
                if (keycode >= Input.Keys.NUM_0 && keycode <= Input.Keys.NUM_9) {
                    textField.append(keycode - 7);
                }
                return false;
            }
        };
        Gdx.input.setInputProcessor(inputAdapter);
    }

    public String getLogin() {
        return textField.toString();
    }

    public void process() {
        var user = menu.getUser();
        processText();
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            var games = Client.getGames();
            if (games != null) {
                menu.getGames().clear();
                menu.getGames().addAll(games);
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            shift = 36;
        } else {
            shift = 68;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.BACKSPACE) && !textField.isEmpty()) {
            textField.setLength(textField.length() - 1);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            if (!textField.isEmpty() && user == null) { // Create user
                user = Client.createUser(textField.toString());
                if (user != null) {
                    menu.setUser(user);
                    textField.setLength(0);
                }
                return;
            }

            if (user != null) { // Authorized
                if (textField.isEmpty()) { // Create game
                    var game = Client.createGame(user.getId());
                    if (game != null) {
                        menu.addGame(game);
                    }
                } else { // Join game
                    var game = tryGetGame();
                    if (game != null) {
                        if (Client.joinGame(game.getGameId(), menu.getUser().getId())) {
                            CardGame.currentScreen = new GamePlay(game, user);
                        }
                    }
                }
            }
        }
    }

    private void processText() {
        var games = menu.getGames();
        var user = menu.getUser();
        StringBuilder gamesList = new StringBuilder();
        for (int i = 0; i < games.size(); i++) {
            gamesList.append(i)
                .append(": ")
                .append(games.get(i).getGameId())
                .append("\n");
        }
        if (user != null) {
            menu.setText(String.format("""
                User: %s
                Press Enter to create game
                Type game number and press Enter to join
                Press space to load games
                Games:
                %s
                """, user.getLogin(), gamesList));
        }
    }

    private Game tryGetGame() {
        try {
            var i = Integer.parseInt(textField.toString());
            return menu.getGames().get(i);
        } catch (Exception ignored) {
        }
        return null;
    }
}
