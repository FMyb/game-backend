package ru.itmo.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;
import java.util.ArrayList;
import java.util.List;
import ru.itmo.game.model.Card;
import ru.itmo.game.model.CardRank;
import ru.itmo.game.model.CardSuit;
import ru.itmo.game.model.Player;
import ru.itmo.game.screen.Menu;
import ru.itmo.game.screen.Screen;

public class CardGame extends ApplicationAdapter {

    public static Screen currentScreen;
    SpriteBatch batch;

    @Override
    public void create() {
        batch = new SpriteBatch();
        currentScreen = new Menu();
    }

    @Override
    public void render() {
        ScreenUtils.clear(0.1f, 0.1f, 0.1f, 1);
        batch.begin();
        currentScreen.process(batch);
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
