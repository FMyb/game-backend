package ru.itmo.game.renderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import ru.itmo.game.model.Card;
import ru.itmo.game.model.GameField;

public class GameRenderer {
    private static final float MIDDLE_X = Gdx.graphics.getWidth() / 2f;

    private static final float MIDDLE_Y = Gdx.graphics.getHeight() / 2f;

    public static void renderGame(Batch batch, GameField gameField) {
        gameField.getFont().draw(batch, gameField.getLog(), 10, Gdx.graphics.getHeight() - 10);
        for (int i = 0; i < gameField.getInAction().size(); i++) {
            var card = gameField.getInAction().get(i);
            if (gameField.isPlayerChecking()) {
                card.setPositionX(alignCenter(card, i, gameField.getInAction().size()));
            } else {
                card.setPositionX(MIDDLE_X + i * 20);
            }
            card.setPositionY(MIDDLE_Y);
            card.setFlipped();
            card.render(batch);
        }
        var player = gameField.currentPlayer();
        if (player != null) {
            var cardsNumber = player.cards().size();
            for (int i = 0; i < cardsNumber; i++) {
                var card = player.cards().get(i);
                card.setPositionX(alignCenter(card, i, cardsNumber));
                card.setPositionY(0);
                card.render(batch);
            }
        }

        if (gameField.isChoosingFace()) {
            for (int i = 0; i < gameField.getChoosingCards().size(); i++) {
                var card = gameField.getChoosingCards().get(i);
                card.setPositionX(alignCenter(card, i, gameField.getChoosingCards().size()));
                card.setPositionY(MIDDLE_Y);
                card.render(batch);
            }
        }

        if (gameField.getChosenFace() != null) {
            var card = gameField.getChosenFace();
            card.setScale(0.5f);
            card.setPositionX(Gdx.graphics.getWidth() - card.getWidth() * 2);
            card.setPositionY(Gdx.graphics.getHeight() - card.getHeight() * 2);
            card.render(batch);
        }

    }

    private static float alignCenter(Card card, int number, int size) {
        var width = card.getWidth();
        return MIDDLE_X - width * size / 2f + width * number;
    }
}
