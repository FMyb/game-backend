package ru.itmo.game.utils;

import com.badlogic.gdx.Gdx;

public class GdxUtils {
    public static int getX() {
        return Gdx.input.getX();
    }

    public static int getY() {
        return Gdx.graphics.getHeight() - Gdx.input.getY();
    }
}
