package ru.itmo.game.screen;

import com.badlogic.gdx.graphics.g2d.Batch;
import java.util.ArrayList;
import java.util.List;
import ru.itmo.game.client.Client;
import ru.itmo.game.client.model.Game;
import ru.itmo.game.client.model.User;
import ru.itmo.game.model.GameField;
import ru.itmo.game.processor.GameProcessor;
import ru.itmo.game.renderer.GameRenderer;

public class GamePlay implements Screen{
    private final GameField gameField;
    private List<User> users;

    public GamePlay(Game game, User user) {
        this.gameField = new GameField(game, user);
        this.users = new ArrayList<>();
        users.add(user);
        Client.getState(game.getGameId());
    }

    @Override
    public void process(Batch batch) {
        GameProcessor.process(gameField);
        GameRenderer.renderGame(batch, gameField);
    }
}
