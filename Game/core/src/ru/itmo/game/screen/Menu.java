package ru.itmo.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import java.util.ArrayList;
import java.util.List;
import ru.itmo.game.client.model.Game;
import ru.itmo.game.client.model.User;
import ru.itmo.game.processor.MenuProcessor;

public class Menu implements Screen {
    private String text = "Enter your name and type Enter to create user:";
    private final MenuProcessor menuProcessor;
    private final BitmapFont font;
    private User user = null;
    private final List<Game> games;

    public Menu() {
        menuProcessor = new MenuProcessor(this);
        games = new ArrayList<>();
        font = new BitmapFont();
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        font.getData().setScale(1.5f);
    }

    @Override
    public void process(Batch batch) {
        font.draw(batch, text, 10, Gdx.graphics.getHeight() - 10);
        font.draw(batch, menuProcessor.getLogin(), 500, Gdx.graphics.getHeight() - 10);
        menuProcessor.process();

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addGame(Game game) {
        games.add(game);
    }

    public List<Game> getGames() {
        return games;
    }

    public void setText(String text) {
        this.text = text;
    }
}
