package ru.itmo.game.screen;

import com.badlogic.gdx.graphics.g2d.Batch;

public interface Screen {
    void process(Batch batch);
}
