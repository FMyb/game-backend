package ru.itmo.game.model;

public enum CardSuit {
    HEARTS,
    DIAMONDS,
    CLUBS,
    SPADES,
}
