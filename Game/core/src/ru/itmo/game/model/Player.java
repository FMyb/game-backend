package ru.itmo.game.model;

import java.util.List;
import java.util.UUID;

public record Player(UUID id, List<Card> cards) {
}
