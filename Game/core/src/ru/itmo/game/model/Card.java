package ru.itmo.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Card {
    private static final Texture FRONT = new Texture("cards.png");
    private static final Texture BACK = new Texture("back.png");
    public static final int WIDTH = 136;
    public static final int HEIGHT = 204;
    private static final int STEP = 11;
    private final CardRank rank;
    private final CardSuit suit;
    private final int id;
    private final Sprite sprite;
    private float positionX;
    private float positionY;
    private boolean isFlipped;
    private boolean isChosen;

    public Card(CardRank rank, CardSuit suit, int id) {
        this.rank = rank;
        this.suit = suit;
        this.id = id;
        this.sprite = new Sprite(
            FRONT,
            regionX() * (WIDTH + STEP),
            suit.ordinal() * (HEIGHT + STEP),
            WIDTH,
            HEIGHT
        );
        positionX = 0;
        positionY = 0;
        isFlipped = false;
    }

    public void choose() {
        isChosen = !isChosen;
    }

    public void flip() {
        if (isFlipped) {
            setNotFlipped();
        } else {
            setFlipped();
        }
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public boolean checkCollision(float x, float y) {
        return x > positionX
            && y > positionY
            && x < positionX + getWidth()
            && y < positionY + getHeight();
    }

    public void render(Batch batch) {
        sprite.setPosition(positionX, positionY);
        if (isChosen) {
            sprite.setY(positionY + 30);
        }
        sprite.draw(batch);
    }


    public CardRank getRank() {
        return rank;
    }

    public CardSuit getSuit() {
        return suit;
    }

    public boolean isFlipped() {
        return isFlipped;
    }

    public void setFlipped() {
        sprite.setTexture(BACK);
        sprite.setRegion(0, 0, WIDTH, HEIGHT);
        isFlipped = true;
    }

    public void setNotFlipped() {
        sprite.setTexture(FRONT);
        sprite.setRegion(
            regionX() * (WIDTH + STEP),
            suit.ordinal() * (HEIGHT + STEP),
            WIDTH,
            HEIGHT
        );
        isFlipped = false;
    }

    private int regionX() {
        return (getRank().ordinal() + 1) % 13;
    }

    public void setChosen(boolean chosen) {
        isChosen = chosen;
    }

    public boolean isChosen() {
        return isChosen;
    }

    public int getId() {
        return id;
    }

    public void setScale(float scale) {
        sprite.setScale(scale);
    }

    public float getWidth() {
        return WIDTH * sprite.getScaleX();
    }

    public float getHeight() {
        return HEIGHT * sprite.getScaleY();
    }
}
