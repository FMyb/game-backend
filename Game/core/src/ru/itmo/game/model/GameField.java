package ru.itmo.game.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.IntStream;
import ru.itmo.game.client.Client;
import ru.itmo.game.client.model.Game;
import ru.itmo.game.client.model.GameStatus;
import ru.itmo.game.client.model.State;
import ru.itmo.game.client.model.StateForUser;
import ru.itmo.game.client.model.User;

public class GameField {
    public static final int TIME_UPDATE = 5000;
    private final Game game;
    private final User user;
    private final BitmapFont font;
    private String log;
    private final Player player; // Current player
    private final List<Card> inAction; // Deck of cards in action
    private final List<Card> allCardsOnTable; // Deck of cards on the table
    private final List<Card> outOfAction; // Deck of cards out of action;
    private boolean isPlayerChecking = false;

    private State currentState = null;
    private StateForUser stateForUser = null;

    private boolean isChoosingFace = false;

    private final List<Card> choosingCards = new ArrayList<>();

    private boolean wasUpdated = false;

    private Long timer;

    private Card chosenFace = null;

    public GameField(Game game, User user) {
        this.game = game;
        this.user = user;
        log = "Press ENTER to start the game";
        inAction = new ArrayList<>();
        allCardsOnTable = new ArrayList<>();
        outOfAction = new ArrayList<>();
        font = new BitmapFont();

        currentState = Client.getState(game.getGameId());
        if (currentState != null) {
            stateForUser = Client.getStateForUser(currentState.getId(), game.getGameId());

        }
        player = new Player(user.getId(), getCardsForPlayer());

        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        font.getData().setScale(1.5f);
        timer = System.currentTimeMillis();

        for (int i = 0; i < 12; i++) {
            choosingCards.add(mapCard(i * 4));
        }
        choosingCards.forEach(it -> it.setScale(0.6f));
    }

    private List<Card> mapCards(int[] cards) {
        var result = new ArrayList<Card>();
        if (cards == null) {
            return result;
        }
        for (int currentCard : cards) {
            result.add(mapCard(currentCard));
        }
        return result;
    }

    public Card mapCard(int card) {
        var suit = CardSuit.values()[card % 4];
        var rank = CardRank.values()[card / 4];
        return new Card(rank, suit, card);
    }

    public List<Card> getCardsForPlayer() {
        return mapCards(stateForUser.getUserCards());
    }

    public List<Card> getCurrentCards() {
        return mapCards(stateForUser.getCurrentCards());
    }

    public List<Card> getAllInRoundCards() {
        return mapCards(stateForUser.getAllInRoundCards());
    }

    public Player currentPlayer() {
        return player;
    }

    public List<Card> getInAction() {
        return inAction;
    }

    public List<Card> getAllCardsOnTable() {
        return allCardsOnTable;
    }

    public List<Card> getOutOfAction() {
        return outOfAction;
    }

    public void setPlayerChecking(boolean playerChecking) {
        isPlayerChecking = playerChecking;
    }

    public boolean isPlayerChecking() {
        return isPlayerChecking;
    }

    public State getCurrentState() {
        return currentState;
    }

    public boolean isCurrentTurn() {
        if (stateForUser.getUserTurn() == null) {
            return false;
        }
        return stateForUser.getUserTurn().equals(user.getId());
    }
    public StateForUser getStateForUser() {
        return stateForUser;
    }

    public void setStateForUser(StateForUser stateForUser) {
        this.stateForUser = stateForUser;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    public Long getTimer() {
        return timer;
    }

    public void setTimer(Long timer) {
        this.timer = timer;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public Game getGame() {
        return game;
    }

    public User getUser() {
        return user;
    }

    public BitmapFont getFont() {
        return font;
    }

    public GameStatus getCurrentStatus() {
        return currentState.getGameStatus();
    }

    public boolean isChoosingFace() {
        return isChoosingFace;
    }

    public void setChoosingFace(boolean choosingFace) {
        isChoosingFace = choosingFace;
    }

    public List<Card> getChoosingCards() {
        return choosingCards;
    }

    public boolean wasUpdated() {
        return wasUpdated;
    }

    public void setUpdated(boolean wasUpdated) {
        this.wasUpdated = wasUpdated;
    }

    public Integer getFaceCard() {
        return currentState.getCardNumberToTransfer();
    }

    public Card getChosenFace() {
        return chosenFace;
    }

    public void setChosenFace(Card chosenFace) {
        this.chosenFace = chosenFace;
    }
}
