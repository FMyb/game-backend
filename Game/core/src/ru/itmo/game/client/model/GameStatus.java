package ru.itmo.game.client.model;

public enum GameStatus {
    INITIAL, IN_PROGRESS, FINISHED
}
