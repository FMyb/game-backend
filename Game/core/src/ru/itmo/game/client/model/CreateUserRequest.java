package ru.itmo.game.client.model;

public record CreateUserRequest(String login) {
}
