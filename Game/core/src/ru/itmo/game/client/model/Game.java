package ru.itmo.game.client.model;

import java.util.Set;
import java.util.UUID;

public class Game {
    private UUID gameId;

    private UUID ownerId;

    private Set<UUID> players;

    private GameSettings settings;

    private GameStatus gameStatus;

    private UUID stateId;

    public UUID getGameId() {
        return gameId;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public Set<UUID> getPlayers() {
        return players;
    }

    public GameSettings getSettings() {
        return settings;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public UUID getStateId() {
        return stateId;
    }
}
