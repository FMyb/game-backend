package ru.itmo.game.client.model;

import java.util.UUID;

public class User {
    private UUID id;
    private String login;

    public UUID getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }
}
