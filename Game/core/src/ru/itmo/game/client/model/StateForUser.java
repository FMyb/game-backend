package ru.itmo.game.client.model;

import java.util.EnumSet;
import java.util.Map;
import java.util.UUID;

public class StateForUser {
    private Map<UUID, Integer> usersCards;
    private int[] droppedCards;
    private GameStatus gameStatus;
    private UUID userTurn;
    private UUID[] players;
    private int[] userCards;
    private EnumSet<MoveType> availableMoves;
    private int[] currentCards;
    private int[] allInRoundCards;

    private Integer cardNumberToTransfer;

    public Map<UUID, Integer> getUsersCards() {
        return usersCards;
    }

    public int[] getDroppedCards() {
        return droppedCards;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public UUID getUserTurn() {
        return userTurn;
    }

    public UUID[] getPlayers() {
        return players;
    }

    public int[] getUserCards() {
        return userCards;
    }

    public EnumSet<MoveType> getAvailableMoves() {
        return availableMoves;
    }

    public int[] getCurrentCards() {
        return currentCards;
    }

    public int[] getAllInRoundCards() {
        return allInRoundCards;
    }

    public Integer getCardNumberToTransfer() {
        return cardNumberToTransfer;
    }
}
