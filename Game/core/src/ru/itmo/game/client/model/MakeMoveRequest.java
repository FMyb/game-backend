package ru.itmo.game.client.model;

import java.util.List;

public record MakeMoveRequest(
    MoveType moveType,
    Integer cardToCheck,
    List<Integer> cardsToTransfer,
    Integer cardNumberToTransfer
) {
}
