package ru.itmo.game.client.model;

import java.util.Map;
import java.util.UUID;

public class State {
    private UUID id;
    private Map<UUID, Integer> usersCards;
    private int[] droppedCards;
    private GameStatus gameStatus;
    private UUID userTurn;
    private UUID[] players;

    private Integer cardNumberToTransfer;

    public UUID getId() {
        return id;
    }

    public Map<UUID, Integer> getUsersCards() {
        return usersCards;
    }

    public int[] getDroppedCards() {
        return droppedCards;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public UUID getUserTurn() {
        return userTurn;
    }

    public UUID[] getPlayers() {
        return players;
    }

    public Integer getCardNumberToTransfer() {
        return cardNumberToTransfer;
    }
}
