package ru.itmo.game.client.model;

public record UpdateGameSettingsRequest(String gameName, int maxCardToTransfer) {
}
