package ru.itmo.game.client.model;

import java.util.UUID;

public record CreateGameRequest(UUID ownerId) {
}
