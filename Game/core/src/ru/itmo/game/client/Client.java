package ru.itmo.game.client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import ru.itmo.game.client.model.CreateGameRequest;
import ru.itmo.game.client.model.CreateUserRequest;
import ru.itmo.game.client.model.Game;
import ru.itmo.game.client.model.MakeMoveRequest;
import ru.itmo.game.client.model.State;
import ru.itmo.game.client.model.StateForUser;
import ru.itmo.game.client.model.User;

public class Client {
    private static final String URL = "http://localhost:8085/";
    private static final Gson gson = new Gson();

    private static <K> K anyRequest(HttpUriRequest request, Class<K> clazz) {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpResponse response = httpClient.execute(request);
            var resultJson = EntityUtils.toString(response.getEntity());
            return gson.fromJson(resultJson, clazz);
        } catch (Exception ignored) {
        }
        return null;
    }

    private static <T> boolean postRequest(String endpoint, T requestBody) {
        var json = gson.toJson(requestBody);
        try {
            HttpPost request = new HttpPost(URL + endpoint);
            request.setHeader("Content-type", "application/json");
            request.setEntity(new StringEntity(json));
            return requestStatus(request);
        } catch (UnsupportedEncodingException ignored) {

        }
        return false;
    }

    private static <T, K> K postRequest(String endpoint, T requestBody, Class<K> clazz) {
        var json = gson.toJson(requestBody);
        try {
            HttpPost request = new HttpPost(URL + endpoint);
            request.setHeader("Content-type", "application/json");
            request.setEntity(new StringEntity(json));
            return anyRequest(request, clazz);
        } catch (UnsupportedEncodingException ignored) {

        }
        return null;
    }

    private static boolean requestStatus(HttpUriRequest request) {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            var status = httpClient.execute(request).getStatusLine();
            if (status.getStatusCode() != 200) {

                System.out.println(status.getReasonPhrase());
                return false;
            }
            return true;
        } catch (Exception ignored) {

        }
        return false;
    }

    public static User createUser(String name) {
        return postRequest("user", new CreateUserRequest(name), User.class);
    }

    public static Game createGame(UUID ownerId) {
        return postRequest("game", new CreateGameRequest(ownerId), Game.class);
    }

    public static boolean makeMove(UUID gameId, MakeMoveRequest makeMoveRequest) {
        return postRequest(String.format("game/%s", gameId), makeMoveRequest);
    }

    public static State getState(UUID gameId) {
        return anyRequest(new HttpGet(URL + String.format("game/state/%s", gameId)), State.class);
    }


    public static StateForUser getStateForUser(UUID id, UUID userId) {
        return anyRequest(
            new HttpGet(URL + String.format("state/%s?user_id=%s", id, userId)),
            StateForUser.class
        );
    }

    public static List<Game> getGames() {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(URL + "game");
            HttpResponse response = httpClient.execute(request);
            var resultJson = EntityUtils.toString(response.getEntity());
            return gson.fromJson(resultJson, new TypeToken<List<Game>>() {
            }.getType());
        } catch (Exception ignored) {

        }
        return null;
    }

    public static boolean joinGame(UUID gameId, UUID userId) {
        return requestStatus(
            new HttpPut(URL + String.format("game/%s/join?user_id=%s", gameId, userId))
        );
    }

    public static boolean startGame(UUID gameId) {
        return requestStatus(
            new HttpPost(URL + String.format("game/%s/start", gameId))
        );
    }
}
