package org.example.pp.service;

import org.example.pp.dto.CreateGameRequest;
import org.example.pp.dto.MakeMoveRequest;
import org.example.pp.exceptions.BadRequestException;
import org.example.pp.exceptions.InvalidMoveTypeException;
import org.example.pp.exceptions.NotFoundException;
import org.example.pp.exceptions.StateForGameNotFoundException;
import org.example.pp.model.*;
import org.example.pp.repository.GameRepository;
import org.example.pp.repository.GameStateRepository;
import org.example.pp.repository.UserCardsRepository;
import org.example.pp.repository.UserRepository;
import org.example.pp.service.game.GameService;
import org.example.pp.service.game.impl.GameServiceImpl;
import org.example.pp.service.state.StateService;
import org.example.pp.service.user.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;

public class GameServiceTest {
    private GameService gameService;
    private UserService userService;
    private StateService stateService;
    private GameRepository gameRepository;
    private GameStateRepository gameStateRepository;

    @BeforeEach
    public void setUp() {
        gameRepository = Mockito.mock(GameRepository.class);
        gameStateRepository = Mockito.mock(GameStateRepository.class);
        userService = Mockito.mock(UserService.class);
        stateService = Mockito.mock(StateService.class);
        gameService = new GameServiceImpl(
                stateService,
                gameRepository,
                gameStateRepository,
                userService
        );
    }

    @Test
    public void createGameTest() {
        var stateId = UUID.randomUUID();
        Mockito.when(userService.createUser("user1"))
                .thenReturn(new User(UUID.randomUUID(), "user1"));
        User user1 = userService.createUser("user1");

        Mockito.when(userService.findUserById(user1.getId())).thenReturn(user1);
        Mockito.when(gameRepository.save(Mockito.any(Game.class)))
                .thenAnswer(obj -> obj.<Game>getArgument(0));
        Mockito.when(stateService.initState(Mockito.any(), Mockito.eq(user1.getId())))
                .thenAnswer(obj -> new GameState(
                        stateId,
                        null,
                        new UUID[]{user1.getId()},
                        0,
                        new int[0],
                        new int[0],
                        -1,
                        new int[0]
                ));
        var game = gameService.createGame(new CreateGameRequest(user1.getId()));
        Mockito.verify(gameRepository, Mockito.times(2)).save(Mockito.any(Game.class));
        var gameSettings = GameSettings.defaultSettings(game.getGameId());
        Assertions.assertNotNull(game);
        Assertions.assertEquals(user1.getId(), game.getOwnerId());
        Assertions.assertEquals(Set.of(user1.getId()), game.getPlayers());
        Assertions.assertEquals(stateId, game.getStateId());
        Assertions.assertEquals(gameSettings.getMaxCardsToTransfer(), game.getSettings().getMaxCardToTransfer());
        Assertions.assertEquals(gameSettings.getName(), game.getSettings().getName());
        Assertions.assertEquals(GameStatus.INITIAL, game.getGameStatus());
    }

    @Test
    public void joinGameTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var fakeId = UUID.randomUUID();
        var gameId = UUID.randomUUID();
        Mockito.when(gameRepository.findById(fakeId)).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class, () -> gameService.joinGame(fakeId, user.getId()));
        Mockito.when(userService.findUserById(user.getId())).thenReturn(user);
        Mockito.when(gameRepository.findById(gameId))
                .thenReturn(Optional.of(new Game(gameId, owner, null, null, null, Set.of(owner))));
        Mockito.when(gameRepository.save(new Game(gameId, owner, null, null, null, Set.of(owner, user))))
                .thenAnswer(obj -> obj.<Game>getArgument(0));
        Assertions.assertDoesNotThrow(() -> gameService.joinGame(gameId, user.getId()));
        Mockito.verify(gameRepository, Mockito.times(1)).save(Mockito.any(Game.class));
    }

    @Test
    public void leaveGameTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var fakeId = UUID.randomUUID();
        var gameId = UUID.randomUUID();
        Mockito.when(gameRepository.findById(fakeId)).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class, () -> gameService.leaveGame(fakeId, user.getId()));
        Mockito.when(userService.findUserById(user.getId())).thenReturn(user);
        Mockito.when(gameRepository.findById(gameId))
                .thenReturn(Optional.of(new Game(gameId, owner, null, null, null, Set.of(owner, user))));
        Mockito.when(gameRepository.save(new Game(gameId, owner, null, null, null, Set.of(owner))))
                .thenAnswer(obj -> obj.<Game>getArgument(0));
        Assertions.assertDoesNotThrow(() -> gameService.joinGame(gameId, user.getId()));
        Mockito.verify(gameRepository, Mockito.times(1)).save(Mockito.any(Game.class));
    }

    @Test
    public void makeMove_GameNotFoundTest() {
        var gameId = UUID.randomUUID();
        Mockito.when(gameRepository.findById(gameId))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(
                NotFoundException.class,
                () -> gameService.makeMove(gameId, null)
        );
    }

    @Test
    public void makeMove_GameNotReadyTest() {
        var gameId = UUID.randomUUID();
        Mockito.when(gameRepository.findById(gameId))
                .thenReturn(Optional.of(new Game(
                        gameId,
                        null,
                        null,
                        GameStatus.INITIAL,
                        null,
                        null
                )));
        Assertions.assertThrows(
                BadRequestException.class,
                () -> gameService.makeMove(gameId, null)
        );
    }

    @Test
    public void makeMove_GameFinishedTest() {
        var gameId = UUID.randomUUID();
        Mockito.when(gameRepository.findById(gameId))
                .thenReturn(Optional.of(new Game(
                        gameId,
                        null,
                        null,
                        GameStatus.FINISHED,
                        null,
                        null
                )));
        Assertions.assertThrows(
                BadRequestException.class,
                () -> gameService.makeMove(gameId, null)
        );
    }

    @Test
    public void makeMove_StateNotFoundTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var gameId = UUID.randomUUID();
        Mockito.when(gameRepository.findById(gameId))
                .thenReturn(Optional.of(new Game(
                        gameId,
                        owner,
                        null,
                        GameStatus.IN_PROGRESS,
                        null,
                        null
                )));
        Mockito.when(gameStateRepository.findByGameId(gameId))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(
                StateForGameNotFoundException.class,
                () -> gameService.makeMove(gameId, null)
        );
    }

    @Test
    public void makeMove_emptyUserCards() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                0,
                new int[0],
                new int[0],
                -1,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[0]);
        Assertions.assertThrows(
                BadRequestException.class,
                () -> gameService.makeMove(game.getId(), null)
        );
        Mockito.verify(stateService, Mockito.times(1)).checkCardToDrop(state.getId());
    }

    @Test
    public void makeMove_NotAvailableMoveTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                0,
                new int[0],
                new int[0],
                -1,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[]{1, 2, 3, 4});
        Mockito.when(stateService.availableMoves(state, owner.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK));
        Assertions.assertThrows(
                BadRequestException.class,
                () -> gameService.makeMove(game.getId(), new MakeMoveRequest(
                        MoveType.TRANSFER,
                        0,
                        null,
                        0
                ))
        );
        Mockito.verify(stateService, Mockito.times(1)).checkCardToDrop(state.getId());
    }

    @Test
    public void makeMove_CurrentCardsEmpty() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                0,
                new int[0],
                new int[0],
                -1,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[]{1, 2, 3, 4});
        Mockito.when(stateService.availableMoves(state, owner.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK));
        Assertions.assertThrows(
                InvalidMoveTypeException.class,
                () -> gameService.makeMove(game.getId(), new MakeMoveRequest(
                        MoveType.CHECK,
                        0,
                        null,
                        0
                ))
        );
        Mockito.verify(stateService, Mockito.times(1)).checkCardToDrop(state.getId());
    }

    @Test
    public void makeMove_BadCardToCheckTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                0,
                new int[0],
                new int[]{1},
                -1,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[]{1, 2, 3, 4});
        Mockito.when(stateService.availableMoves(state, owner.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK));
        Assertions.assertThrows(
                BadRequestException.class,
                () -> gameService.makeMove(game.getId(), new MakeMoveRequest(
                        MoveType.TRANSFER,
                        1,
                        null,
                        0
                ))
        );
        Mockito.verify(stateService, Mockito.times(1)).checkCardToDrop(state.getId());
    }

    @Test
    public void makeMove_SuccessCheckCardTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                0,
                new int[]{1, 2, 3, 4},
                new int[]{5, 6, 7, 8},
                4,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[]{1, 2, 3, 4});
        Mockito.when(stateService.availableMoves(state, owner.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK));
        Mockito.when(stateService.checkFinished(state.getId()))
                .thenReturn(false);
        gameService.makeMove(game.getId(), new MakeMoveRequest(
                MoveType.CHECK,
                3,
                null,
                0
        ));
        Mockito.verify(stateService, Mockito.times(1)).checkCardToDrop(state.getId());
        Mockito.verify(stateService, Mockito.times(1))
                .addCardsToPlayer(state.getId(), user.getId(), 0, new int[]{1, 2, 3, 4, 5, 6, 7, 8});
    }

    @Test
    public void makeMove_FailCheckCardTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                0,
                new int[]{1, 2, 3, 4},
                new int[]{5, 6, 7, 8},
                4,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[]{10, 11, 12, 13});
        Mockito.when(stateService.availableMoves(state, owner.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK));
        Mockito.when(stateService.checkFinished(state.getId()))
                .thenReturn(true);
        gameService.makeMove(game.getId(), new MakeMoveRequest(
                MoveType.CHECK,
                0,
                null,
                0
        ));
        Mockito.verify(stateService, Mockito.times(1)).checkCardToDrop(state.getId());
        Mockito.verify(stateService, Mockito.times(1))
                .addCardsToPlayer(state.getId(), owner.getId(), 1, new int[]{1, 2, 3, 4, 5, 6, 7, 8});
        Mockito.verify(stateService, Mockito.times(1))
                .removePlayerIfEmptyCards(state.getId(), user.getId());
        Mockito.verify(gameRepository, Mockito.times(1))
                .save(game);
        Assertions.assertEquals(GameStatus.FINISHED, game.getStatus());
    }

    @Test
    public void makeMove_NextPlayerUserCardsEmptyTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                1,
                new int[]{1, 2, 3, 4},
                new int[]{5, 6, 7, 8},
                4,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), user.getId()))
                .thenReturn(new int[]{10, 11, 12, 13});
        Mockito.when(stateService.availableMoves(state, user.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK, MoveType.TRANSFER));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[0]);
        Assertions.assertThrows(
                InvalidMoveTypeException.class,
                () -> gameService.makeMove(game.getId(), new MakeMoveRequest(
                        MoveType.TRANSFER,
                        0,
                        List.of(10, 11, 12, 13),
                        10
                ))
        );
    }

    @Test
    public void makeMove_TooManyCardsTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        game.setSettings(GameSettings.defaultSettings(game.getId()));
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                1,
                new int[]{1, 2, 3, 4},
                new int[]{5, 6, 7, 8},
                4,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), user.getId()))
                .thenReturn(new int[]{10, 11, 12, 13, 14, 15, 16, 17});
        Mockito.when(stateService.availableMoves(state, user.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK, MoveType.TRANSFER));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[]{18, 19, 20, 21});
        Assertions.assertThrows(
                BadRequestException.class,
                () -> gameService.makeMove(game.getId(), new MakeMoveRequest(
                        MoveType.TRANSFER,
                        0,
                        List.of(10, 11, 12, 13, 14, 15, 16, 17),
                        10
                ))
        );
    }

    @Test
    public void makeMove_TransferSuccessTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        game.setSettings(GameSettings.defaultSettings(game.getId()));
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                1,
                new int[]{1, 2, 3, 4},
                new int[]{5, 6, 7, 8},
                4,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state));
        Mockito.when(stateService.userCards(state.getId(), user.getId()))
                .thenReturn(new int[]{10, 11, 12, 13, 14, 15, 16, 17});
        Mockito.when(stateService.availableMoves(state, user.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK, MoveType.TRANSFER));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[]{18, 19, 20, 21});
        Mockito.when(stateService.checkFinished(state.getId()))
                .thenReturn(false);
        gameService.makeMove(game.getId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                0,
                List.of(10, 11, 12, 13),
                10
        ));
        Mockito.verify(stateService, Mockito.times(1))
                .makeTransfer(
                        state.getId(),
                        user.getId(),
                        0,
                        10,
                        new int[]{1, 2, 3, 4, 5, 6, 7, 8},
                        List.of(10, 11, 12, 13)
                );
    }

    @Test
    public void makeMove_TransferPlayerRemoveTest() {
        var owner = new User(UUID.randomUUID(), "owner");
        var user = new User(UUID.randomUUID(), "user");
        var user2 = new User(UUID.randomUUID(), "user2");
        var game = new Game(
                UUID.randomUUID(),
                null,
                null,
                GameStatus.IN_PROGRESS,
                null,
                null
        );
        game.setSettings(GameSettings.defaultSettings(game.getId()));
        Mockito.when(gameRepository.findById(game.getId()))
                .thenReturn(Optional.of(game));
        var state = new GameState(
                UUID.randomUUID(),
                game,
                new UUID[]{owner.getId(), user.getId(), user2.getId()},
                1,
                new int[]{1, 2, 3, 4},
                new int[]{5, 6, 7, 8},
                4,
                new int[0]
        );
        var modifiedState = new GameState(
                state.getId(),
                game,
                new UUID[]{owner.getId(), user.getId()},
                1,
                new int[]{1, 2, 3, 4},
                new int[]{5, 6, 7, 8},
                4,
                new int[0]
        );
        Mockito.when(gameStateRepository.findByGameId(game.getId()))
                .thenReturn(Optional.of(state))
                .thenReturn(Optional.of(modifiedState));
        Mockito.when(stateService.userCards(state.getId(), user.getId()))
                .thenReturn(new int[]{10, 11, 12, 13, 14, 15, 16, 17});
        Mockito.when(stateService.availableMoves(state, user.getId()))
                .thenReturn(EnumSet.of(MoveType.CHECK, MoveType.TRANSFER));
        Mockito.when(stateService.userCards(state.getId(), owner.getId()))
                .thenReturn(new int[]{18, 19, 20, 21});
        Mockito.when(stateService.checkFinished(state.getId()))
                .thenReturn(false);
        gameService.makeMove(game.getId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                0,
                List.of(10, 11, 12, 13),
                10
        ));
        Mockito.verify(stateService, Mockito.times(1))
                .makeTransfer(
                        state.getId(),
                        user.getId(),
                        0,
                        10,
                        new int[]{1, 2, 3, 4, 5, 6, 7, 8},
                        List.of(10, 11, 12, 13)
                );
        Mockito.verify(stateService, Mockito.times(1))
                .removePlayerIfEmptyCards(state.getId(), user2.getId());
    }
}
