package org.example.pp.scenario;

import org.example.pp.controller.GameController;
import org.example.pp.controller.StateController;
import org.example.pp.controller.UserController;
import org.example.pp.dto.CreateGameRequest;
import org.example.pp.dto.CreateUserDTO;
import org.example.pp.dto.MakeMoveRequest;
import org.example.pp.model.*;
import org.example.pp.repository.GameStateRepository;
import org.example.pp.repository.UserCardsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.simple.JdbcClient;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.*;

@SpringBootTest
class GameScenarioTest {
    @Autowired
    GameController gameController;

    @Autowired
    UserController userController;

    @Autowired
    StateController stateController;

    @Autowired
    GameStateRepository gameStateRepository;

    @Autowired
    UserCardsRepository userCardsRepository;

    @Autowired
    private JdbcClient jdbcClient;

    @BeforeEach
    void setUp() {
        JdbcTestUtils.deleteFromTables(
                jdbcClient,
                "game_settings",
                "game_users",
                "user_cards",
                "games_state",
                "games",
                "users"
        );
    }

    void setState(GameState gameState, List<UserCards> userCards) {
        gameStateRepository.saveAndFlush(gameState);
        userCardsRepository.saveAllAndFlush(userCards);
    }

    @Test
    void twoPlayerSimpleScenario() {
        var owner = userController.createUser(new CreateUserDTO("owner"));
        Assertions.assertEquals("owner", owner.getLogin());

        var game = gameController.createGame(new CreateGameRequest(owner.getId()));
        Assertions.assertEquals(GameStatus.INITIAL, game.getGameStatus());
        Assertions.assertEquals(Set.of(owner.getId()), game.getPlayers());
        Assertions.assertEquals(owner.getId(), game.getOwnerId());
        Assertions.assertEquals("New Game", game.getSettings().getName());
        Assertions.assertEquals(
                6,
                game.getSettings().getMaxCardToTransfer()
        );

        var player2 = userController.createUser(new CreateUserDTO("player2"));
        Assertions.assertEquals("player2", player2.getLogin());

        gameController.joinGame(game.getGameId(), player2.getId());
        game = gameController.getGame(game.getGameId());
        Assertions.assertEquals(Set.of(owner.getId(), player2.getId()), game.getPlayers());

        gameController.startGame(game.getGameId());

        game = gameController.getGame(game.getGameId());
        var state = gameController.getState(game.getGameId());
        Assertions.assertEquals(state.getId(), game.getStateId());
        Assertions.assertEquals(52, state.getUsersCards().values().stream().mapToInt(v -> v).sum() + state.getDroppedCards().length);
        Assertions.assertEquals(2, state.getPlayers().length);
        Assertions.assertEquals(GameStatus.IN_PROGRESS, game.getGameStatus());
        Assertions.assertEquals(GameStatus.IN_PROGRESS, state.getGameStatus());

        var user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(2, user1State.getPlayers().length);
        Assertions.assertEquals(2, user1State.getUsersCards().size());
        Assertions.assertEquals(0, user1State.getAllInRoundCards().length);
        Assertions.assertEquals(0, user1State.getCurrentCards().length);
        Assertions.assertEquals(GameStatus.IN_PROGRESS, user1State.getGameStatus());
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER), user1State.getAvailableMoves());

        var user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(2, user2State.getPlayers().length);
        Assertions.assertEquals(2, user2State.getUsersCards().size());
        Assertions.assertEquals(0, user2State.getAllInRoundCards().length);
        Assertions.assertEquals(0, user2State.getCurrentCards().length);
        Assertions.assertEquals(GameStatus.IN_PROGRESS, user2State.getGameStatus());
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER), user2State.getAvailableMoves());

        var tmp = new Game();
        tmp.setId(game.getGameId());
        setState(
                new GameState(
                        state.getId(),
                        tmp,
                        new UUID[]{owner.getId(), player2.getId()},
                        0,
                        new int[0],
                        new int[0],
                        -1,
                        new int[]{12, 13, 14, 15, 24, 25, 26, 27}
                ),
                List.of(
                        new UserCards(state.getId(), owner.getId(), new int[]{37, 20, 9, 51, 36, 22, 38, 33, 7, 4, 10, 46, 17, 0, 2, 41, 30, 31, 5, 49, 40, 28}),
                        new UserCards(state.getId(), player2.getId(), new int[]{21, 47, 50, 6, 1, 34, 8, 23, 29, 39, 11, 48, 42, 16, 43, 35, 3, 19, 32, 44, 45, 18})
                )
        );

        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(state.getId(), game.getStateId());
        Assertions.assertEquals(22, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(22, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(List.of(owner.getId(), player2.getId()), Arrays.stream(state.getPlayers()).toList());
        Assertions.assertEquals(owner.getId(), state.getUserTurn());
        Assertions.assertEquals(GameStatus.IN_PROGRESS, game.getGameStatus());
        Assertions.assertEquals(GameStatus.IN_PROGRESS, state.getGameStatus());

        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(22, user1State.getUserCards().length);

        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(22, user2State.getUserCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(0, 2, 4, 5, 7, 9),
                4
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(16, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(22, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(player2.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(6, user1State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user1State.getAvailableMoves());
        Assertions.assertEquals(6, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(6, user2State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user2State.getAvailableMoves());
        Assertions.assertEquals(6, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(1, 3, 6, 8, 11, 16),
                null
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(16, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(16, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(owner.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(6, user1State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user1State.getAvailableMoves());
        Assertions.assertEquals(12, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(6, user2State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user2State.getAvailableMoves());
        Assertions.assertEquals(12, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(10, 17, 20, 22, 28, 30),
                null
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(10, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(16, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(player2.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(6, user1State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user1State.getAvailableMoves());
        Assertions.assertEquals(18, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(6, user2State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user2State.getAvailableMoves());
        Assertions.assertEquals(18, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(18, 19, 21, 23, 29, 32),
                null
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(10, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(10, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(owner.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(6, user1State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user1State.getAvailableMoves());
        Assertions.assertEquals(24, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(6, user2State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user2State.getAvailableMoves());
        Assertions.assertEquals(24, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(31, 33, 36, 37, 38, 40),
                null
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(4, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(10, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(player2.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(6, user1State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user1State.getAvailableMoves());
        Assertions.assertEquals(30, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(6, user2State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user2State.getAvailableMoves());
        Assertions.assertEquals(30, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(34, 35, 39, 42, 43, 44),
                null
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(4, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(4, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(owner.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(6, user1State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user1State.getAvailableMoves());
        Assertions.assertEquals(36, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(6, user2State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER, MoveType.CHECK), user2State.getAvailableMoves());
        Assertions.assertEquals(36, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(41, 46, 49, 51),
                null
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(0, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(4, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(player2.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(4, user1State.getCurrentCards().length);
        Assertions.assertEquals(0, user1State.getAvailableMoves().size());
        Assertions.assertEquals(40, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(4, user2State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.CHECK), user2State.getAvailableMoves());
        Assertions.assertEquals(40, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.CHECK,
                0,
                null,
                null
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(8, state.getDroppedCards().length);
        Assertions.assertEquals(40, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(4, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(player2.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(0, user1State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER), user1State.getAvailableMoves());
        Assertions.assertEquals(0, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(0, user2State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.TRANSFER), user2State.getAvailableMoves());
        Assertions.assertEquals(0, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(45, 47, 48, 50),
                45
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(44, state.getDroppedCards().length);
        Assertions.assertEquals(4, state.getUsersCards().get(owner.getId()));
        Assertions.assertEquals(0, state.getUsersCards().get(player2.getId()));
        Assertions.assertEquals(owner.getId(), state.getUserTurn());
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(4, user1State.getCurrentCards().length);
        Assertions.assertEquals(EnumSet.of(MoveType.CHECK), user1State.getAvailableMoves());
        Assertions.assertEquals(4, user1State.getAllInRoundCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(4, user2State.getCurrentCards().length);
        Assertions.assertEquals(0, user2State.getAvailableMoves().size());
        Assertions.assertEquals(4, user2State.getAllInRoundCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.CHECK,
                0,
                null,
                null
        ));
        state = gameController.getState(game.getGameId());
        Assertions.assertEquals(GameStatus.FINISHED, state.getGameStatus());
        game = gameController.getGame(game.getGameId());
        Assertions.assertEquals(GameStatus.FINISHED, game.getGameStatus());
    }

    @Test
    void checkFailScenario() {
        var owner = userController.createUser(new CreateUserDTO("owner"));
        var game = gameController.createGame(new CreateGameRequest(owner.getId()));
        var player2 = userController.createUser(new CreateUserDTO("player2"));
        gameController.joinGame(game.getGameId(), player2.getId());
        game = gameController.getGame(game.getGameId());
        gameController.startGame(game.getGameId());
        game = gameController.getGame(game.getGameId());
        var state = gameController.getState(game.getGameId());
        var tmp = new Game();
        tmp.setId(game.getGameId());
        setState(
                new GameState(
                        state.getId(),
                        tmp,
                        new UUID[]{owner.getId(), player2.getId()},
                        0,
                        new int[0],
                        new int[0],
                        -1,
                        new int[]{12, 13, 14, 15, 24, 25, 26, 27}
                ),
                List.of(
                        new UserCards(state.getId(), owner.getId(), new int[]{37, 20, 9, 51, 36, 22, 38, 33, 7, 4, 10, 46, 17, 0, 2, 41, 30, 31, 5, 49, 40, 28}),
                        new UserCards(state.getId(), player2.getId(), new int[]{21, 47, 50, 6, 1, 34, 8, 23, 29, 39, 11, 48, 42, 16, 43, 35, 3, 19, 32, 44, 45, 18})
                )
        );

        state = gameController.getState(game.getGameId());

        var user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(22, user1State.getUserCards().length);

        var user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(22, user2State.getUserCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(0, 2, 4, 5, 7, 9),
                4
        ));
        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.CHECK,
                0,
                null,
                null
        ));
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(22, user1State.getUserCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(22, user2State.getUserCards().length);
    }

    @Test
    void checkSuccessScenario() {
        var owner = userController.createUser(new CreateUserDTO("owner"));
        var game = gameController.createGame(new CreateGameRequest(owner.getId()));
        var player2 = userController.createUser(new CreateUserDTO("player2"));
        gameController.joinGame(game.getGameId(), player2.getId());
        game = gameController.getGame(game.getGameId());
        gameController.startGame(game.getGameId());
        game = gameController.getGame(game.getGameId());
        var state = gameController.getState(game.getGameId());
        var tmp = new Game();
        tmp.setId(game.getGameId());
        setState(
                new GameState(
                        state.getId(),
                        tmp,
                        new UUID[]{owner.getId(), player2.getId()},
                        0,
                        new int[0],
                        new int[0],
                        -1,
                        new int[]{12, 13, 14, 15, 24, 25, 26, 27}
                ),
                List.of(
                        new UserCards(state.getId(), owner.getId(), new int[]{37, 20, 9, 51, 36, 22, 38, 33, 7, 4, 10, 46, 17, 0, 2, 41, 30, 31, 5, 49, 40, 28}),
                        new UserCards(state.getId(), player2.getId(), new int[]{21, 47, 50, 6, 1, 34, 8, 23, 29, 39, 11, 48, 42, 16, 43, 35, 3, 19, 32, 44, 45, 18})
                )
        );

        state = gameController.getState(game.getGameId());

        var user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(22, user1State.getUserCards().length);

        var user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(22, user2State.getUserCards().length);

        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.TRANSFER,
                null,
                List.of(0, 2, 4, 5, 7, 9),
                4
        ));
        gameController.makeMove(game.getGameId(), new MakeMoveRequest(
                MoveType.CHECK,
                2,
                null,
                null
        ));
        user1State = stateController.getStateForUser(state.getId(), owner.getId());
        Assertions.assertEquals(16, user1State.getUserCards().length);
        user2State = stateController.getStateForUser(state.getId(), player2.getId());
        Assertions.assertEquals(28, user2State.getUserCards().length);
    }
}
