package org.example.pp.configuration;

import org.example.pp.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.Map;

@ControllerAdvice
public class ExceptionsControllerAdvice {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> handleException(BadRequestException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.badRequest().body(Map.of("error", message));
    }

    @ExceptionHandler(InvalidMoveTypeException.class)
    public ResponseEntity<?> handleException(InvalidMoveTypeException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.badRequest().body(Map.of("error", message));
    }

    @ExceptionHandler(StateForGameNotFoundException.class)
    public ResponseEntity<?> handleException(StateForGameNotFoundException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("error", message));
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<?> handleException(InternalServerErrorException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.internalServerError().body(Map.of("error", message));
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> handleException(NotFoundException exception) {
        String message = String.format("%s: %s", LocalDateTime.now(), exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("error", message));
    }
}
