package org.example.pp.repository;

import org.example.pp.model.Game;
import org.example.pp.model.GameStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface GameRepository extends JpaRepository<Game, UUID> {
    GameStatus findGameStatusById(UUID id);
}
