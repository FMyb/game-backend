package org.example.pp.repository;

import org.example.pp.model.UserCards;
import org.example.pp.model.UserCardsId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public interface UserCardsRepository extends JpaRepository<UserCards, UserCardsId> {
    Optional<UserCards> findByUserIdAndStateId(UUID userId, UUID stateId);

    List<UserCards> findAllByStateId(UUID stateId);

    default Map<UUID, Integer> findAllUserCardsSizesByStateId(UUID stateId) {
        return findAllByStateId(stateId).stream()
                .collect(Collectors.toMap(UserCards::getUserId, v -> v.getUserCards().length));
    }
}
