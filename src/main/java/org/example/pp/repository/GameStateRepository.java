package org.example.pp.repository;

import org.example.pp.model.GameState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface GameStateRepository extends JpaRepository<GameState, UUID> {
    @Query("from gameState gs join game g where g.id = :gameId")
    Optional<GameState> findByGameId(@Param("gameId") UUID gameId);

    @Query("from gameState gs join game g where gs.id = :id")
    Optional<GameState> findGameStateById(@Param("id") UUID id);
}
