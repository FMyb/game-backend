package org.example.pp.controller;

import java.util.List;
import org.example.pp.dto.*;
import org.example.pp.service.game.GameService;
import org.example.pp.service.state.StateService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/game")
public class GameController {
    private final GameService gameService;
    private final StateService stateService;

    public GameController(
            GameService gameService,
            StateService stateService
    ) {
        this.gameService = gameService;
        this.stateService = stateService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public GameDTO createGame(@RequestBody CreateGameRequest createGameRequest) {
        return gameService.createGame(createGameRequest);
    }

    @PutMapping(value = "/{id}/join")
    public void joinGame(@PathVariable("id") UUID gameId, @RequestParam(name = "user_id") UUID userId) {
        gameService.joinGame(gameId, userId);
    }

    @DeleteMapping(value = "/{id}/leave")
    public void leaveGame(@PathVariable("id") UUID gameId, @RequestParam(name = "user_id") UUID userId) {
        gameService.leaveGame(gameId, userId);
    }

    @GetMapping(value = "/state/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public StateDTO getState(@PathVariable("id") UUID gameId) {
        return stateService.getState(gameId);
    }

    @PostMapping(value = "/{id}/start")
    public void startGame(@PathVariable("id") UUID gameId) {
        gameService.startGame(gameId);
    }

    @PostMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void makeMove(@PathVariable("id") UUID gameId, @RequestBody MakeMoveRequest makeMoveRequest) {
        gameService.makeMove(gameId, makeMoveRequest);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GameDTO getGame(@PathVariable("id") UUID gameId) {
        return gameService.getGame(gameId);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GameDTO> getGames() {
        return gameService.getGames();
    }
}
