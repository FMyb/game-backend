package org.example.pp.controller;

import org.example.pp.dto.CreateUserDTO;
import org.example.pp.dto.UserDTO;
import org.example.pp.service.user.UserService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO createUser(@RequestBody CreateUserDTO createUserDTO) {
        var user =  userService.createUser(createUserDTO.getLogin());
        return new UserDTO(user.getId(), user.getLogin());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO getUser(@PathVariable("id") UUID id) {
        var user =  userService.findUserById(id);
        return new UserDTO(user.getId(), user.getLogin());
    }
}
