package org.example.pp.controller;

import org.example.pp.dto.StateForUserDTO;
import org.example.pp.service.state.StateService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/state")
public class StateController {
    private final StateService stateService;

    public StateController(StateService stateService) {
        this.stateService = stateService;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public StateForUserDTO getStateForUser(@PathVariable("id") UUID id, @RequestParam("user_id") UUID userId) {
        return stateService.getStateForUser(id, userId);
    }
}
