package org.example.pp.exceptions;

import org.example.pp.model.MoveType;

public class InvalidMoveTypeException extends RuntimeException {
    public InvalidMoveTypeException(MoveType moveType, String reason) {
        super(String.format("Move type '%s' is not valid. Reason: %s", moveType, reason));
    }
}
