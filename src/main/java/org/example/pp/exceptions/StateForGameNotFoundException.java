package org.example.pp.exceptions;

import java.util.UUID;

public class StateForGameNotFoundException extends RuntimeException {
    public StateForGameNotFoundException(UUID gameId) {
        super(String.format("State for game: %s not found", gameId));
    }
}
