package org.example.pp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.pp.model.GameStatus;
import org.example.pp.model.MoveType;

import java.util.EnumSet;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StateForUserDTO {
    private Map<UUID, Integer> usersCards;
    private int[] droppedCards;
    private GameStatus gameStatus;
    private UUID userTurn;
    private UUID[] players;
    private int[] userCards;
    private EnumSet<MoveType> availableMoves;
    private int[] currentCards;
    private int[] allInRoundCards;
    private Integer cardNumberToTransfer;
}
