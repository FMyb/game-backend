package org.example.pp.dto;

import lombok.*;
import org.example.pp.model.GameStatus;

import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GameDTO {
    private UUID gameId;

    private UUID ownerId;

    private Set<UUID> players;

    private GameSettingsDTO settings;

    private GameStatus gameStatus;

    private UUID stateId;
}
