package org.example.pp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.pp.model.MoveType;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MakeMoveRequest {
    private MoveType moveType;
    private Integer cardToCheck;
    private List<Integer> cardsToTransfer;
    private Integer cardNumberToTransfer;
}
