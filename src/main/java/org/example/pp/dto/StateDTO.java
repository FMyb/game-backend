package org.example.pp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.pp.model.GameStatus;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StateDTO {
    private UUID id;
    private Map<UUID, Integer> usersCards;
    private int[] droppedCards;
    private GameStatus gameStatus;
    private UUID userTurn;
    private UUID[] players;
    private Integer cardNumberToTransfer;
}
