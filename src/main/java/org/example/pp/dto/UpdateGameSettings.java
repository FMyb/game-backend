package org.example.pp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateGameSettings {
    private String gameName;

    private int maxCardToTransfer;
}
