package org.example.pp.model;

import io.hypersistence.utils.hibernate.type.array.IntArrayType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.util.UUID;

@Entity(name = "userCards")
@Table(name = "user_cards")
@IdClass(UserCardsId.class)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserCards {
    @Id
    @Column(name = "state_id")
    private UUID stateId;

    @Id
    @Column(name = "user_id")
    private UUID userId;

    @Type(IntArrayType.class)
    @Column(name = "user_cards", columnDefinition = "int[]")
    private int[] userCards;
}
