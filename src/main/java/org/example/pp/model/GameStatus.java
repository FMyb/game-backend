package org.example.pp.model;

public enum GameStatus {
    INITIAL, IN_PROGRESS, FINISHED
}
