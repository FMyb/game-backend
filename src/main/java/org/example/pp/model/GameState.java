package org.example.pp.model;

import io.hypersistence.utils.hibernate.type.array.IntArrayType;
import io.hypersistence.utils.hibernate.type.array.UUIDArrayType;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Type;

import java.util.List;
import java.util.UUID;

@Entity(name = "gameState")
@Table(name = "games_state")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class GameState {
    @Id
    private UUID id;

    @OneToOne
    @JoinColumn(name = "game_id")
    private Game game;

    @Type(UUIDArrayType.class)
    @Column(columnDefinition = "uuid[]")
    private UUID[] players;

    @Column(name = "current_player")
    private Integer currentPlayer;

    @Type(IntArrayType.class)
    @Column(name = "prev_cards", columnDefinition = "int[]")
    private int[] prevCards;

    @Type(IntArrayType.class)
    @Column(name = "current_cards", columnDefinition = "int[]")
    private int[] currentCards;

    @Column(name = "card_num_to_check")
    private Integer cardNumberToCheck;

    @Type(IntArrayType.class)
    private int[] droppedCards;
}
