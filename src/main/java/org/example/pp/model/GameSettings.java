package org.example.pp.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Random;
import java.util.UUID;

@Entity(name = "gameSettings")
@Table(name = "game_settings")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GameSettings {
    public static GameSettings defaultSettings(UUID gameId) {
        return new GameSettings(
                gameId,
                "New Game",
                6
        );
    }

    @Id
    @Column(name = "id")
    private UUID gameId;

    private String name;

    @Column(name = "card_to_transfer")
    private int maxCardsToTransfer;
}
