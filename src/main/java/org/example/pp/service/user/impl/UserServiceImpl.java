package org.example.pp.service.user.impl;

import org.example.pp.dto.UserDTO;
import org.example.pp.exceptions.NotFoundException;
import org.example.pp.model.User;
import org.example.pp.repository.UserRepository;
import org.example.pp.service.user.UserService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(String login) {
        return userRepository.save(new User(UUID.randomUUID(), login));
    }

    @Override
    public User findUserById(UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("User with id %s not found", id)));
    }
}
