package org.example.pp.service.user;

import org.example.pp.dto.UserDTO;
import org.example.pp.model.User;

import java.util.UUID;

public interface UserService {
    User createUser(String login);

    User findUserById(UUID id);
}
