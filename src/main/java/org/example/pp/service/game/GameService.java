package org.example.pp.service.game;

import java.util.List;
import java.util.UUID;
import org.example.pp.dto.CreateGameRequest;
import org.example.pp.dto.GameDTO;
import org.example.pp.dto.MakeMoveRequest;

public interface GameService {
    GameDTO createGame(CreateGameRequest createGameRequest);

    void joinGame(UUID gameId, UUID userId);

    GameDTO startGame(UUID gameId);

    void makeMove(UUID gameId, MakeMoveRequest makeMoveRequest);

    GameDTO getGame(UUID gameId);

    List<GameDTO> getGames();

    void leaveGame(UUID gameId, UUID userId);
}
