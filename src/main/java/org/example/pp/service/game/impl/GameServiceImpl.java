package org.example.pp.service.game.impl;

import java.util.ArrayList;
import java.util.List;
import org.example.pp.dto.CreateGameRequest;
import org.example.pp.dto.GameDTO;
import org.example.pp.dto.GameSettingsDTO;
import org.example.pp.dto.MakeMoveRequest;
import org.example.pp.exceptions.BadRequestException;
import org.example.pp.exceptions.InvalidMoveTypeException;
import org.example.pp.exceptions.NotFoundException;
import org.example.pp.exceptions.StateForGameNotFoundException;
import org.example.pp.model.*;
import org.example.pp.repository.GameRepository;
import org.example.pp.repository.GameStateRepository;
import org.example.pp.service.game.GameService;
import org.example.pp.service.state.StateService;
import org.example.pp.service.user.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class GameServiceImpl implements GameService {
    private final StateService stateService;
    private final GameRepository gameRepository;
    private final GameStateRepository gameStateRepository;
    private final UserService userService;

    public GameServiceImpl(
            StateService stateService,
            GameRepository gameRepository,
            GameStateRepository gameStateRepository,
            UserService userService
    ) {
        this.stateService = stateService;
        this.gameRepository = gameRepository;
        this.gameStateRepository = gameStateRepository;
        this.userService = userService;
    }

    @Override
    @Transactional
    public GameDTO createGame(CreateGameRequest createGameRequest) {
        var gameId = UUID.randomUUID();
        User owner = userService.findUserById(createGameRequest.getOwnerId());
        var game = gameRepository.save(Game.builder()
                .id(gameId)
                .owner(owner)
                .players(Set.of(owner))
                .status(GameStatus.INITIAL)
                .build()
        );
        var initState = stateService.initState(gameId, game.getOwner().getId());
        game.setState(initState);
        game.setSettings(GameSettings.defaultSettings(gameId));
        gameRepository.save(game);
        return new GameDTO(
                gameId,
                owner.getId(),
                game.getPlayers().stream().map(User::getId).collect(Collectors.toSet()),
                new GameSettingsDTO(
                        game.getSettings().getName(),
                        game.getSettings().getMaxCardsToTransfer()
                ),
                game.getStatus(),
                initState.getId()
        );
    }

    @Override
    @Transactional
    public void joinGame(UUID gameId, UUID userId) {
        Game game = gameRepository.findById(gameId)
                .orElseThrow(() -> new NotFoundException(String.format("Game: %s not found", gameId)));
        User user = userService.findUserById(userId);
        var players = new HashSet<>(game.getPlayers());
        players.add(user);
        game.setPlayers(players);
        gameRepository.save(game);
    }

    @Override
    @Transactional
    public void leaveGame(UUID gameId, UUID userId) {
        Game game = gameRepository.findById(gameId)
                .orElseThrow(() -> new NotFoundException(String.format("Game: %s not found", gameId)));
        User user = userService.findUserById(userId);
        var players = new HashSet<>(game.getPlayers());
        players.remove(user);
        game.setPlayers(players);
        gameRepository.save(game);
    }

    @Override
    public GameDTO startGame(UUID gameId) {
        var game = gameRepository.findById(gameId)
                .orElseThrow(() -> new NotFoundException(String.format("Game: %s not found", gameId)));
        var state = stateService.startGame(gameId);
        game.setState(state);
        game.setStatus(GameStatus.IN_PROGRESS);
        game = gameRepository.save(game);
        return new GameDTO(
                game.getId(),
                game.getOwner().getId(),
                game.getPlayers().stream().map(User::getId).collect(Collectors.toSet()),
                new GameSettingsDTO(
                        game.getSettings().getName(),
                        game.getSettings().getMaxCardsToTransfer()
                ),
                game.getStatus(),
                state.getId()
        );
    }

    @Override
    @Transactional
    public void makeMove(UUID gameId, MakeMoveRequest makeMoveRequest) {
        var game = gameRepository.findById(gameId)
                .orElseThrow(() -> new NotFoundException(String.format("game: %s not found", gameId)));
        if (game.getStatus() != GameStatus.IN_PROGRESS) {
            throw new BadRequestException("Game is not in progress");
        }
        var currentState = gameStateRepository.findByGameId(gameId)
                        .orElseThrow(() -> new StateForGameNotFoundException(gameId));
        stateService.checkCardToDrop(currentState.getId());
        var currentPlayer = currentState.getCurrentPlayer();
        var players = currentState.getPlayers();
        if (stateService.userCards(currentState.getId(), players[currentPlayer]).length == 0) {
            throw new BadRequestException("Current player don't have any cards");
        }
        if (!stateService.availableMoves(currentState, players[currentPlayer]).contains(makeMoveRequest.getMoveType())) {
            throw new BadRequestException(String.format("Current player can not make '%s' move", makeMoveRequest.getMoveType()));
        }
        var prevPlayer = players[(currentPlayer - 1 + players.length) % players.length];
        var prevCards = currentState.getPrevCards();
        var currentCards = currentState.getCurrentCards();
        var allInRoundCards = Stream.concat(
                        Arrays.stream(prevCards == null ? new int[0] : prevCards).boxed(),
                        Arrays.stream(currentCards == null ? new int[0] : currentCards).boxed()
                )
                .mapToInt(Integer::intValue)
                .toArray();
        switch (makeMoveRequest.getMoveType()) {
            case CHECK -> makeCheck(
                    makeMoveRequest,
                    currentState,
                    players,
                    currentCards,
                    currentPlayer,
                    prevPlayer,
                    allInRoundCards
            );
            case TRANSFER -> makeTransfer(
                    game,
                    makeMoveRequest,
                    currentState,
                    players,
                    currentPlayer,
                    allInRoundCards
            );
        }
        if (stateService.checkFinished(currentState.getId())) {
            game.setStatus(GameStatus.FINISHED);
            gameRepository.save(game);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public GameDTO getGame(UUID gameId) {
        var game = gameRepository.findById(gameId)
                .orElseThrow(() -> new NotFoundException(String.format("game: %s not found", gameId)));
        var state = stateService.getState(gameId);
        return new GameDTO(
                game.getId(),
                game.getOwner().getId(),
                game.getPlayers().stream().map(User::getId).collect(Collectors.toSet()),
                new GameSettingsDTO(
                        game.getSettings().getName(),
                        game.getSettings().getMaxCardsToTransfer()
                ),
                game.getStatus(),
                state.getId()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public List<GameDTO> getGames() {
        var games = gameRepository.findAll();
        var result = new ArrayList<GameDTO>();
        for (Game game : games) {
            var state = stateService.getState(game.getId());
            result.add(new GameDTO(
                game.getId(),
                game.getOwner().getId(),
                game.getPlayers().stream().map(User::getId).collect(Collectors.toSet()),
                new GameSettingsDTO(
                    game.getSettings().getName(),
                    game.getSettings().getMaxCardsToTransfer()
                ),
                game.getStatus(),
                state.getId()
            ));
        }
       return result;
    }

    private void makeCheck(
            MakeMoveRequest makeMoveRequest,
            GameState currentState,
            UUID[] players,
            int[] currentCards,
            Integer currentPlayer,
            UUID prevPlayer,
            int[] allInRoundCards
    ) {
        if (currentCards == null || currentCards.length == 0) {
            throw new InvalidMoveTypeException(MoveType.CHECK, "no cards in round");
        }
        if (makeMoveRequest.getCardToCheck() < 0 || makeMoveRequest.getCardToCheck() > currentCards.length) {
            throw new InvalidMoveTypeException(MoveType.CHECK, "card to check is not valid");
        }
        var cardToCheck = currentCards[makeMoveRequest.getCardToCheck()];
        if (cardToCheck / 4 == currentState.getCardNumberToCheck() / 4) {
            stateService.addCardsToPlayer(
                    currentState.getId(),
                    players[currentPlayer],
                    (currentPlayer + 1) % players.length,
                    allInRoundCards
            );
            stateService.removePlayerIfEmptyCards(currentState.getId(), prevPlayer);
        } else {
            stateService.addCardsToPlayer(
                    currentState.getId(),
                    prevPlayer,
                    currentPlayer,
                    allInRoundCards
            );
        }
    }

    private void makeTransfer(
            Game game,
            MakeMoveRequest makeMoveRequest,
            GameState currentState,
            UUID[] players,
            Integer currentPlayer,
            int[] allInRoundCards
    ) {
        if (players.length > 2) {
            var playerToRemove = players[(currentPlayer - 2 + players.length) % players.length];
            stateService.removePlayerIfEmptyCards(currentState.getId(), playerToRemove);
            currentState = gameStateRepository.findByGameId(game.getId())
                    .orElseThrow(() -> new StateForGameNotFoundException(game.getId()));
            currentPlayer = currentState.getCurrentPlayer();
            players = currentState.getPlayers();
        }
        var nextPlayer = players[(currentPlayer + 1) % players.length];
        if (stateService.userCards(currentState.getId(), nextPlayer).length == 0) {
            throw new InvalidMoveTypeException(MoveType.TRANSFER, "next player don't have cards");
        }
        var faceValue = makeMoveRequest.getCardNumberToTransfer();
        var cardsToTransfer = makeMoveRequest.getCardsToTransfer();
        if (cardsToTransfer.size() > game.getSettings().getMaxCardsToTransfer()) {
            throw new BadRequestException("cards to transfer exceeds max cards to transfer");
        }
        stateService.makeTransfer(currentState.getId(), players[currentPlayer],(currentPlayer + 1) % players.length, faceValue, allInRoundCards, cardsToTransfer);
    }
}
