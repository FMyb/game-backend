package org.example.pp.service.state;

import org.example.pp.dto.StateDTO;
import org.example.pp.dto.StateForUserDTO;
import org.example.pp.model.GameState;
import org.example.pp.model.MoveType;

import java.util.EnumSet;
import java.util.List;
import java.util.UUID;

public interface StateService {
    GameState makeTransfer(UUID stateId, UUID currentPlayer, int nextPlayer, Integer faceValue, int[] prevCards, List<Integer> cardsToCheck);

    StateDTO getState(UUID gameId);

    GameState addCardsToPlayer(UUID stateId, UUID player, Integer nextPlayer, int[] cards);

    GameState changeTurn(UUID stateID, Integer nextPlayer);

    int[] userCards(UUID stateId, UUID player);

    boolean checkAvailableTransferToOther(UUID stateID);

    void removePlayerIfEmptyCards(UUID stateId, UUID player);

    StateForUserDTO getStateForUser(UUID id, UUID userId);

    void checkCardToDrop(UUID stateId);

    GameState initState(UUID gameId, UUID ownerId);

    GameState startGame(UUID gameId);

    boolean checkFinished(UUID stateId);

    EnumSet<MoveType> availableMoves(GameState gameState, UUID userId);
}
