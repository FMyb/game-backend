package org.example.pp.service.state.impl;

import org.example.pp.dto.StateDTO;
import org.example.pp.dto.StateForUserDTO;
import org.example.pp.exceptions.BadRequestException;
import org.example.pp.exceptions.InternalServerErrorException;
import org.example.pp.exceptions.NotFoundException;
import org.example.pp.exceptions.StateForGameNotFoundException;
import org.example.pp.model.GameState;
import org.example.pp.model.MoveType;
import org.example.pp.model.User;
import org.example.pp.model.UserCards;
import org.example.pp.repository.GameRepository;
import org.example.pp.repository.GameStateRepository;
import org.example.pp.repository.UserCardsRepository;
import org.example.pp.service.state.StateService;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
public class StateServiceImpl implements StateService {
    private final GameStateRepository gameStateRepository;
    private final UserCardsRepository userCardsRepository;
    private final GameRepository gameRepository;

    public StateServiceImpl(
            GameStateRepository gameStateRepository,
            UserCardsRepository userCardsRepository,
            GameRepository gameRepository
    ) {
        this.gameStateRepository = gameStateRepository;
        this.userCardsRepository = userCardsRepository;
        this.gameRepository = gameRepository;
    }

    @Override
    @Transactional
    public GameState makeTransfer(UUID stateId, UUID currentPlayer, int nextPlayer, Integer faceValue, int[] prevCards, List<Integer> cardsToCheck) {
        if (faceValue != null && faceValue == 12) {
            throw new BadRequestException("faceValue is required and must be not Ace");
        }
        var state = gameStateRepository.findGameStateById(stateId)
                .orElseThrow(() -> new NotFoundException(String.format("Game state with id %s not found", stateId)));
        if (faceValue != null && state.getCardNumberToCheck() != -1 && !faceValue.equals(state.getCardNumberToCheck())) {
            throw new BadRequestException("faceValue is must be equal with a card number to transfer on first move in this round");
        }
        if (faceValue == null && state.getCardNumberToCheck() == -1) {
            throw new BadRequestException("faceValue is required and must be not Ace");
        }
        var userCards = userCardsRepository.findByUserIdAndStateId(currentPlayer, stateId)
                .orElseThrow(() -> new NotFoundException(String.format("cards for user: %s in state %s not found", currentPlayer, stateId)));
        var userCardsSet = Arrays.stream(userCards.getUserCards()).boxed().collect(Collectors.toSet());
        if (!userCardsSet.containsAll(cardsToCheck)) {
            throw new BadRequestException(String.format("player %s try to throw unavailable cards", currentPlayer));
        }
        userCards.setUserCards(Arrays.stream(userCards.getUserCards()).filter(i -> !cardsToCheck.contains(i)).toArray());
        userCardsRepository.save(userCards);
        state.setCurrentCards(cardsToCheck.stream().mapToInt(Integer::intValue).toArray());
        state.setPrevCards(prevCards);
        state.setCurrentPlayer(nextPlayer);
        if (faceValue != null) {
            state.setCardNumberToCheck(faceValue);
        }
        return gameStateRepository.save(state);
    }

    @Override
    @Transactional(readOnly = true)
    public StateDTO getState(UUID gameId) {
        var state = gameStateRepository.findByGameId(gameId)
                .orElseThrow(() -> new StateForGameNotFoundException(gameId));
        var usersCards = userCardsRepository.findAllUserCardsSizesByStateId(state.getId());
        UUID currentPlayer = null;
        if (state.getPlayers().length > 1) {
            currentPlayer = state.getPlayers()[state.getCurrentPlayer()];
        }
        return new StateDTO(
                state.getId(),
                usersCards,
                state.getDroppedCards(),
                state.getGame().getStatus(),
                currentPlayer,
                state.getPlayers(),
                state.getCardNumberToCheck() == -1 ? null : state.getCardNumberToCheck()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public StateForUserDTO getStateForUser(UUID id, UUID userId) {
        var state = gameStateRepository.findGameStateById(id)
                .orElseThrow(() -> new NotFoundException(String.format("state %s not found", id)));
        var usersCards = userCardsRepository.findAllUserCardsSizesByStateId(id);
        if (usersCards.isEmpty()) {
            throw new IllegalArgumentException("No user cards");
        }
        var availableMoves = availableMoves(state, userId);
        var userCards = userCardsRepository.findByUserIdAndStateId(userId, id)
                .orElseThrow(() -> new NotFoundException(String.format("not found user %s cards in state %s", userId, id)));
        var userCardsArr = userCards.getUserCards();
        Arrays.sort(userCardsArr);
        return new StateForUserDTO(
                usersCards,
                state.getDroppedCards(),
                state.getGame().getStatus(),
                state.getPlayers()[state.getCurrentPlayer()],
                state.getPlayers(),
                userCardsArr,
                availableMoves,
                state.getCurrentCards(),
                Stream.concat(Arrays.stream(state.getCurrentCards()).boxed(), Arrays.stream(state.getPrevCards()).boxed())
                        .mapToInt(Integer::intValue)
                        .toArray(),
                state.getCardNumberToCheck() == -1 ? null : state.getCardNumberToCheck()
        );
    }

    @Override
    @Transactional
    public void checkCardToDrop(UUID stateId) {
        var cards = userCardsRepository.findAllByStateId(stateId);
        if (cards.isEmpty()) {
            throw new BadRequestException("No cards for users in state " + stateId);
        }
        var state = gameStateRepository.findGameStateById(stateId)
                .orElseThrow(() -> new NotFoundException(String.format("state %s not found", stateId)));
        List<Integer> droppedCards = new ArrayList<>();
        for (var userCard : cards) {
            var newCards = dropUserCards(stateId, userCard.getUserId(), userCard.getUserCards());
            userCard.setUserCards(newCards.getFirst());
            droppedCards.addAll(newCards.getSecond());
            userCardsRepository.save(userCard);
        }
        state.setDroppedCards(
                Stream.concat(Arrays.stream(state.getDroppedCards()).boxed(), droppedCards.stream())
                        .sorted()
                        .mapToInt(Integer::intValue)
                        .toArray()
        );
        gameStateRepository.save(state);
    }

    @Override
    @Transactional
    public GameState initState(UUID gameId, UUID ownerId) {
        var game = gameRepository.findById(gameId)
                .orElseThrow(() -> new NotFoundException(String.format("game %s not found", gameId)));
        return gameStateRepository.save(GameState.builder()
                .id(UUID.randomUUID())
                .game(game)
                .currentCards(new int[0])
                .droppedCards(new int[0])
                .players(new UUID[]{ownerId})
                .prevCards(new int[0])
                .cardNumberToCheck(-1)
                .currentPlayer(0)
                .build()
        );
    }

    @Override
    @Transactional
    public GameState startGame(UUID gameId) {
        var state = gameStateRepository.findByGameId(gameId)
                .orElseThrow(() -> new StateForGameNotFoundException(gameId));
        var players = new ArrayList<>(state.getGame().getPlayers());
        Collections.shuffle(players);
        state.setPlayers(players.stream().map(User::getId).toArray(UUID[]::new));
        state.setCurrentPlayer(0);
        state.setPrevCards(new int[0]);
        state.setCurrentCards(new int[0]);
        state.setDroppedCards(new int[0]);
        state.setCardNumberToCheck(-1);
        gameStateRepository.save(state);
        var cards = new ArrayList<>(IntStream.range(0, 52).boxed().toList());
        Collections.shuffle(cards);
        var userCards = Arrays.stream(state.getPlayers())
                .collect(Collectors.toMap(k -> k, __ -> new ArrayList<Integer>()));
        for (int card = 0, curUser = 0; card < cards.size(); card++, curUser = (curUser + 1) % players.size()) {
            int finalCard = card;
            userCards.computeIfPresent(players.get(curUser).getId(), (__, v) -> {
                v.add(cards.get(finalCard));
                return v;
            });
        }
        var result = userCards.entrySet().stream()
                .map(it -> new UserCards(
                        state.getId(),
                        it.getKey(),
                        it.getValue().stream().mapToInt(Integer::intValue).toArray())
                ).toList();
        userCardsRepository.saveAll(result);
        checkCardToDrop(state.getId());
        return gameStateRepository.findGameStateById(state.getId())
                .orElseThrow(() -> new InternalServerErrorException(String.format("state: %s must be present", state.getId())));
    }

    @Override
    @Transactional
    public GameState addCardsToPlayer(UUID stateId, UUID player, Integer nextPlayer, int[] cards) {
        var playerCards = userCardsRepository.findByUserIdAndStateId(player, stateId)
                .orElseThrow(() -> new NotFoundException(String.format("cards for user %s not found in state %s", player, stateId)));
        var state = gameStateRepository.findGameStateById(stateId)
                .orElseThrow(() -> new NotFoundException(String.format("game %s not found", stateId)));
        var allCards = Stream.concat(
                Arrays.stream(playerCards.getUserCards()).boxed(),
                Arrays.stream(cards).boxed()
        ).mapToInt(Integer::intValue).toArray();
        playerCards.setUserCards(allCards);
        userCardsRepository.save(playerCards);
        state.setCurrentCards(new int[0]);
        state.setPrevCards(new int[0]);
        state.setCardNumberToCheck(-1);
        return changeTurn(stateId, nextPlayer);
    }

    @Override
    @Transactional
    public GameState changeTurn(UUID stateID, Integer nextPlayer) {
        var state = gameStateRepository.findGameStateById(stateID)
                .orElseThrow(() -> new NotFoundException(String.format("state %s not found", stateID)));
        state.setCurrentPlayer(nextPlayer);
        return gameStateRepository.save(state);
    }

    @Override
    @Transactional(readOnly = true)
    public int[] userCards(UUID stateId, UUID player) {
        var userCards = userCardsRepository.findByUserIdAndStateId(player, stateId)
                .orElseThrow(() -> new NotFoundException(String.format("cards for user %s not found in state %s", player, stateId)));
        return userCards.getUserCards();
    }

    @Override
    @Transactional(readOnly = true)
    public boolean checkAvailableTransferToOther(UUID stateID) {
        return userCardsRepository.findAllUserCardsSizesByStateId(stateID)
                .values()
                .stream()
                .mapToInt(v -> v > 0 ? 1 : 0)
                .sum() > 1;
    }

    @Override
    @Transactional
    public void removePlayerIfEmptyCards(UUID stateId, UUID player) {
        var userCards = userCardsRepository.findByUserIdAndStateId(player, stateId)
                .orElseThrow(() -> new NotFoundException(String.format("cards for user %s not found in state %s", player, stateId)));
        if (userCards.getUserCards().length == 0) {
            var state = gameStateRepository.findGameStateById(stateId)
                    .orElseThrow(() -> new InternalServerErrorException("state must be present"));
            state.setPlayers(Arrays.stream(state.getPlayers())
                    .filter(it -> !it.equals(player))
                    .toArray(UUID[]::new)
            );
            gameStateRepository.save(state);
        }
    }

    @Transactional(readOnly = true)
    public EnumSet<MoveType> availableMoves(GameState state, UUID userId) {
        var availableMoves = EnumSet.noneOf(MoveType.class);
        var userCards = userCardsRepository.findByUserIdAndStateId(userId, state.getId())
                .orElseThrow(() -> new NotFoundException(String.format("not found user %s cards in state %s", userId, state.getId())));
        if (state.getCurrentCards().length > 0 && userCards.getUserCards().length > 0) {
            availableMoves.add(MoveType.CHECK);
        }
        if (userCards.getUserCards().length > 0 && checkAvailableTransferToOther(state.getId())) {
            availableMoves.add(MoveType.TRANSFER);
        }
        return availableMoves;
    }

    public boolean checkFinished(UUID stateId) {
        var state = gameStateRepository.findGameStateById(stateId)
                .orElseThrow(() -> new NotFoundException(String.format("game state %s not found", stateId)));
        return state.getPlayers().length == 1;
    }

    private Pair<int[], List<Integer>> dropUserCards(UUID stateId, UUID player, int[] cards) {
        Arrays.sort(cards);
        if (Arrays.stream(cards).boxed().collect(Collectors.toSet()).size() != cards.length) {
            throw new InternalServerErrorException(String.format("not unique card in state: %s, player: %s, cards: %s", stateId, player, Arrays.toString(cards)));
        }
        var mapped = Arrays.stream(cards)
                .boxed()
                .collect(Collectors.groupingBy(k -> k / 4, Collectors.toList()));
        List<Integer> resultCards = new ArrayList<>();
        List<Integer> droppedCards = new ArrayList<>();
        for (var entry : mapped.entrySet()) {
            if (entry.getValue().size() == 4) {
                droppedCards.addAll(entry.getValue());
                continue;
            }
            resultCards.addAll(entry.getValue());
        }
        return Pair.of(
                resultCards.stream().mapToInt(Integer::intValue).toArray(),
                droppedCards
        );
    }
}
