create table if not exists users
(
    id    uuid primary key,
    login varchar(255) not null
);

create table if not exists game_settings
(
    id               uuid         not null primary key,
    name             varchar(255) not null,
    card_to_transfer int          not null default 6
);

create type game_status as enum ('INITIAL', 'IN_PROGRESS', 'FINISHED');

create table if not exists games
(
    id       uuid        not null primary key,
    owner_id uuid        not null references users (id),
    status   game_status not null
);

alter table game_settings
    add constraint fk_game_settings_games foreign key (id) references games (id);

create table if not exists game_users
(
    game_id uuid not null references games (id),
    user_id uuid not null references users (id),
    primary key (game_id, user_id)
);

create index on game_users (user_id, game_id);

create table if not exists games_state
(
    id                uuid primary key,
    game_id           uuid unique not null references games (id),
    players           uuid[]      not null,
    current_player    int         not null,
    prev_cards        int[]       not null,
    current_cards     int[]       not null,
    card_num_to_check int         not null,
    dropped_cards     int[]       not null
);

create table if not exists user_cards
(
    state_id   uuid  not null references games_state (id),
    user_id    uuid  not null references users (id),
    user_cards int[] not null,
    primary key (state_id, user_id)
);
